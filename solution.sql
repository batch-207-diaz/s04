--solution.sql

--a
SELECT * FROM artists WHERE name LIKE "%d%";

--b
SELECT * FROM albums WHERE album_title LIKE "b%";

--c
SELECT * FROM artists 
JOIN albums ON artists.id = albums.artist_id 
WHERE name LIKE "a%" 

--d
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

--e
SELECT * FROM artists ORDER BY name ASC LIMIT 5;