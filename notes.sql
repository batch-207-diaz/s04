--notes.sql

INSERT INTO artists(name)
VALUES
("Taylor Swift"),
("Lady Gaga"),
("Justin Bieber"),
("Ariana Grande"),
("Bruno Mars");

INSERT INTO albums(album_title,date_released, artist_id)
VALUES 
("Fearless","2008-1-1",6),
("Red","2012-1-1",6);

INSERT INTO songs (song_name,length,genre,album_id)
VALUES 
("Fearless",246,"Pop Rock",6),
("Love Story",213,"Country Pop",6),
("State of Grace",313,"Rock, Alternative Rock, Arena Rock",7),
("Red",204,"Country",7);

INSERT INTO albums (album_title,date_released,artist_id)
VALUES
("A Star Is Born","2018-1-1",7),
("Born This Way","2011-1-1",7);

INSERT INTO songs (song_name,length,genre,album_id)
VALUES 
("Black Eyes",221,"Rock and Roll",8),
("Shallow",201,"Country, Rock, Folk Rock",8),
("Born This Way",252,"Electropop",9);

INSERT INTO albums (album_title,date_released,artist_id)
VALUES
("Purpose","2015-1-1",8),
("Believe","2012-1-1",8);

INSERT INTO songs (song_name,length,genre,album_id)
VALUES 
("Sorry",232,"Dancehall-poptropical, housemoombahton",10),
("Boyfriend",251,"Pop",11);

INSERT INTO albums (album_title,date_released,artist_id)
VALUES
("Dangerous Woman","2016-1-1",9),
("Thank U, Next","2019-1-1",9);

INSERT INTO songs (song_name,length,genre,album_id)
VALUES 
("Into You",242,"EDM House",12),
("Thank U, Next",236,"Pop, R&B",13);

INSERT INTO albums (album_title,date_released,artist_id)
VALUES
("24K Magic","2016-1-1",10),
("Earth to Mars","2011-1-1",10);

INSERT INTO songs (song_name,length,genre,album_id)
VALUES 
("24K Magic",207,"Funk Disco, R&B",14),
("Lost",232,"Pop",15);


---


SELECT * FROM songs WHERE id != 11;
SELECT * FROM songs WHERE id != 7 AND album_id != 8;

SELECT * FROM songs WHERE id = 7 OR id = 8 OR id = 9;
SELECT * FROM songs WHERE id IN (7,8,9); -- in clause

-- "%" wildcard operator
SELECT * FROM songs WHERE song_name LIKE "th%";
SELECT * FROM songs WHERE song_name LIKE "%ce";
SELECT * FROM songs WHERE song_name LIKE "Bo%";
SELECT * FROM songs WHERE song_name LIKE "%a%";

-- "_" wildcard operator
SELECT * FROM songs WHERE song_name LIKE "_r";
SELECT * FROM songs WHERE song_name LIKE "__rr_";
SELECT * FROM songs WHERE song_name LIKE "__r%";

-- ORDER BY
SELECT * FROM songs ORDER BY song_name;
SELECT * FROM songs ORDER BY song_name DESC;
SELECT * FROM songs ORDER BY length DESC;

-- LIMIT
SELECT * FROM songs LIMIT 5;


-- JOIN
SELECT * FROM artists 
JOIN albums ON artists.id = albums.artist_id
WHERE name LIKE "%a%";

SELECT * FROM artists
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

SELECT name,album_title,date_released,song_name,length, genre
FROM artists
JOIN albums ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;



INSERT INTO users (username,password,full_name,contact_number,emailaddress)
VALUES ("jsmith99","jsmith1234","John Smith", 09266772401, "jsmith@gmail.com", "New York");

INSERT INTO playlists(user_id,datetime_created)
VALUES(4,"2021-01-02 01:00:00");

INSERT INTO playlists_songs (playlist_id,song_id)
VALUES
(1,1),(1,10),(1,11);


SELECT * FROM playlists
JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id
JOIN songs ON playlists_songs.song_id = songs.id;

SELECT user_id, datetime_created, song_name, length, genre, album_id
FROM playlists
JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id
JOIN songs ON playlists_songs.song_id = songs.id;

SELECT username, datetime_created, song_name, length, genre
FROM playlists
JOIN users ON playlists.user_id = users.id
JOIN playlists_songs ON playlists.id = playlists_songs.playlist_id
JOIN songs ON playlists_songs.song_id = songs.id;